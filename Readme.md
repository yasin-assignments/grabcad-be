# mathgame-be

[1]: https://github.com/yasinaydinnet/mathgame-fe

This is the frontend of [mathgame-fe][1] project. 

## Project Info

Technologies used:
- Node.js
- Yarn
- Jest
- Websocket (`ws` package)

About project:
- Only connectivity tests were provided for now

## Development

Note: Instructions here are provided for Linux/Unix/Mac.

Requirements:
- Git
- Node.js
- Yarn (you can also use NPM, but I personally prefer Yarn)

To develop and run the project locally, do the following steps in a shell.

```sh
git clone git@github.com:yasinaydinnet/mathgame-be.git
cd mathgame-be
yarn
yarn start
```

Default address of backend should be http://localhost:8666/ (or ws://localhost:8666/)

## Deploying to Production

This app is already configured to be auto-deployed to a Heroku free-tier machine and is residing at https://mathgame-be.herokuapp.com/
