const WebSocket = require("ws");
const { PORT } = require("../src/conf");

describe('app', () => {
  it('should connect to ws', async done => {
      expect.assertions(1);

      const ws = new WebSocket(`ws://localhost:${PORT}`)

      ws.on('message', msg => {
        expect(msg).toEqual("hi!");
        ws.close();
      })

      ws.on('close', () => done());
  });
});
