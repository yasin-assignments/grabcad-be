# Specs

Reqs:
- [x] github
- [x] node.js
- [x] vue.js
- [x] Chrome browser
- [ ] automated test coverage
- [ ] installation scripts

---

Game:
- Real-time, browser-based game
- 10 concurrent users
- Continues and unlimited series of rounds
- A player can join anytime

Rounds:
- Every round, question is sent to players
- Question is single math challenge with basic operation (+ - * /) and a potential answer
- Players answer using a simple yes/no choice

Answering:
- First answer gets 1 point
- Other answers get 0 point
- Incorrect answers get -1 point
- New round in 5 second
- Users are notified that round is over and next one starts
