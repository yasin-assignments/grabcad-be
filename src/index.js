const WebSocket = require("ws");
const { startARound, joinRound, checkAnswer, leaveGame } = require("./lib/game");
const { genRandomId } = require("./lib/ws");
const { scores, SERVER_LIMIT } = require("./lib/globals");
const { PORT } = require("./conf");

const wss = new WebSocket.Server({ port: PORT });

wss.on("connection", socket => {
  // Rate limit check
  const numberOfPlayers = Object.keys(scores).length;
  if (numberOfPlayers >= SERVER_LIMIT) {
    return socket.close(4001, "SERVER_FULL");
  }

  // Giving id to track scores
  const id = genRandomId();
  socket._id = id;

  joinRound(wss, socket);  

  socket.on("message", answer => checkAnswer(wss, socket, answer))
  socket.on("close", () => leaveGame(wss, id));
});

startARound(wss);
