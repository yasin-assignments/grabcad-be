const genRandomInt = max => Math.floor(Math.random() * Math.floor(max));

const genRandomBool = () => !!genRandomInt(2);

const operators = {
  "+": (a,b) => parseInt(a, 10) + parseInt(b, 10),
  "-": (a,b) => parseInt(a, 10) - parseInt(b, 10),
  "x": (a,b) => parseInt(a, 10) * parseInt(b, 10),
  "/": (a,b) => parseInt(a, 10) / parseInt(b, 10),
}

const genQuestion = () => {
  const arg1 = genRandomInt(11);
  const arg2 = genRandomInt(10) + 1; // To prevent division by zero
  const arg3 = genRandomInt(11);

  answer = genRandomBool();
  
  const opIndex = genRandomInt(Object.keys(operators).length)
  const opSign = Object.keys(operators)[opIndex];
  
  const resultRight = Math.round(100 * operators[opSign](arg1, arg2)) / 100;
  const resultWrong = Math.round(100 * operators[opSign](arg3, arg2)) / 100;
  const resultToAsk = answer ? resultRight : resultWrong;

  const question = `${arg1} ${opSign} ${arg2} = ${resultToAsk}`;

  return { question,answer };
}

module.exports = {
  genQuestion
}
