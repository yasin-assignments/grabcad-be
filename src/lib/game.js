const { genQuestion } = require("./question");
const { broadcastMessage, sendMessage } = require("./ws");
const { scores, round, NEW_ROUND_WAIT_MS } = require("./globals");

const startARound = (wss) => {
  const { question, answer } = genQuestion();

  round.question = question;
  round.answer = answer;
  round.playable = true
  round.answerers = [];

  console.log("[DEBUG] Round updated: %o", round)

  broadcastMessage(wss, {
    playable: round.playable,
    question: round.question,
    answer: null,
    answered: null
  })
}

const leaveGame = (wss, socketId) => {
  console.log("[DEBUG] client %s left", socketId)

  delete scores[socketId];

  broadcastMessage(wss, {
    scores
  })
}

const joinRound = (wss, socket) => {
  console.log("[DEBUG] client %s joined", socket._id)

  scores[socket._id] = 0

  broadcastMessage(wss, {
    scores,
  })

  sendMessage(socket, {
    playable: round.playable,
    id: socket._id,
  })

  if (round.playable) {
    sendMessage(socket, {
      answered: false,
      question: round.question
    })
  }
}

const initiateNewRound = (wss) => {
  broadcastMessage(wss, {
    playable: round.playable,
    scores,
    answer: round.answer
  })

  setTimeout(() => {
    startARound(wss)
  }, NEW_ROUND_WAIT_MS)
}

const newRoundByExh = (wss) => {
  if (Object.keys(scores).length != round.answerers.length) {
    return;
  }

  initiateNewRound(wss);
}

const checkAnswer = (wss, socket, message) => {
  const answer = JSON.parse(message).answer;

  console.log("[DEBUG] client %s answered %s", socket._id, answer)

  if (!round.playable) {
    return newRoundByExh(wss);
  }
  
  if (round.answerers.includes(socket._id)) {
    return newRoundByExh(wss);
  }

  // Mark player as answered
  round.answerers.push(socket._id);
  sendMessage(socket, {
    answered: true
  })

  if (answer != round.answer) {
    scores[socket._id]--;

    broadcastMessage(wss, {
      scores
    })

    return newRoundByExh(wss);
  };

  if (answer == round.answer) {
    round.playable = false

    scores[socket._id]++;

    return initiateNewRound(wss)
  }
}

module.exports = {
  startARound,
  joinRound,
  checkAnswer,
  leaveGame,
};
