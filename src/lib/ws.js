const crypto = require("crypto");

const genRandomId = () => crypto.randomBytes(4).toString("hex");

const sendMessage = (socket, message) => {
  console.log("[DEBUG] message to %s: %o", socket._id, message);

  socket.send(JSON.stringify(message))
}

const broadcastMessage = (server, message) => {
  console.log("[DEBUG] broadcast: %o", message);

  server.clients.forEach(socket => sendMessage(socket, message));
}

module.exports = {
  genRandomId,
  sendMessage,
  broadcastMessage
}
