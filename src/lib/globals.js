module.exports = {
  round: {
    playable: false,
    question: null,
    answer: null,
    answerers: []
  },
  scores: {},
  // Conf
  SERVER_LIMIT: 10,
  NEW_ROUND_WAIT_MS: 5000,
}
